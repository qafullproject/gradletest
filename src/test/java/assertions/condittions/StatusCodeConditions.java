package assertions.condittions;

import assertions.Condition;
import io.restassured.response.ValidatableResponse;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Assertions;

@RequiredArgsConstructor
public class StatusCodeConditions implements Condition {
    private final Integer code;
    @Override
    public void Check(ValidatableResponse response) {
        int actualStatus = response.extract().statusCode();
        Assertions.assertEquals(code, actualStatus);
    }
}
