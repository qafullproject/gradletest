package assertions;

import assertions.condittions.MessageCondition;
import assertions.condittions.StatusCodeConditions;

public class Conditions {
    public static MessageCondition hasMessage(String expectedMessage){
        return new MessageCondition(expectedMessage);
    }

    public static StatusCodeConditions hasStatusCode(Integer expectedStatus){
        return new StatusCodeConditions(expectedStatus);
    }
}
