package models.fakeapiuser;

public class AuthData {
    private String userName;
    private String password;

    public AuthData(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }
}
