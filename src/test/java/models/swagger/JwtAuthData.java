package models.swagger;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.junit.jupiter.api.Test;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JwtAuthData{

	@JsonProperty("username")
	private String username;

	@JsonProperty("password")
	private String password;

}