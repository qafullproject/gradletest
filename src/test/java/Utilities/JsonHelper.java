package Utilities;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;

import java.io.File;
import java.io.IOException;

public class JsonHelper {
    private static final ObjectMapper mapper = new ObjectMapper();

    @SneakyThrows
    public static <T> T fromJsonFile(String jsonPath, Class<T> out) {
            return mapper.readValue(new File(jsonPath), out);
    }

    @SneakyThrows
    public static <T> T fromJsonString(String json, Class<T> out) {
        return mapper.readValue(json, out);
    }

    public static String toJson(Object object){
        try{
            return mapper.writeValueAsString(object);
        }
        catch(Exception e){
            throw new RuntimeException();
        }
    }
 }
