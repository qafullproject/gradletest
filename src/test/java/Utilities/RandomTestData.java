package Utilities;

import com.github.javafaker.Faker;
import models.swagger.*;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

import static assertions.Conditions.hasMessage;
import static assertions.Conditions.hasStatusCode;

public class RandomTestData {
    private static Random rand = new Random();
    private static Faker faker = new Faker();

    public static FullUser getRandomUserWithGames(){
        int random = Math.abs(rand.nextInt());

        SimilarDlc similarDlc = SimilarDlc.builder()
                .isFree(false)
                .dlcNameFromAnotherGame(faker.funnyName().name())
                .build();

        DlcsItem dlcsItem = DlcsItem.builder()
                .rating(faker.random().nextInt(10))
                .price(faker.random().nextInt(200))
                .description(faker.funnyName().name())
                .dlcName(faker.dragonBall().character())
                .isDlcFree(false)
                .similarDlc(similarDlc)
                .build();

        Requirements requirements = Requirements.builder()
                .ramGb(faker.random().nextInt(10))
                .osName("Windows")
                .hardDrive(faker.random().nextInt(10))
                .videoCard("NVIDIA")
                .build();

        GamesItem gamesItem = GamesItem.builder()
                .requirements(requirements)
                .gameId(10)
                .genre(faker.book().genre())
                .price(rand.nextInt(400))
                .description("CS GO")
                .company(faker.company().name())
                .isFree(false)
                .title(faker.beer().name())
                .rating(faker.random().nextInt(10))
                .publishDate(LocalDateTime.now().toString())
                .requiredAge(false)
                .tags(Arrays.asList("sdsd", "sdsds"))
                .dlcs(Collections.singletonList(dlcsItem))
                .build();

        FullUser user = FullUser.builder()
                .login(faker.name().username() + random)
                .pass(faker.internet().password())
                .games(Collections.singletonList(gamesItem))
                .build();

        return user;
    }

    public static FullUser getRandomUser(){
        int random = Math.abs(rand.nextInt());

        return FullUser.builder()
                .login("testToday" + random)
                .pass("passsQqwqw")
                .build();
    }

    public static FullUser getAdminUser(){
        return FullUser.builder()
                .login("admin")
                .pass("admin")
                .build();
    }

}
