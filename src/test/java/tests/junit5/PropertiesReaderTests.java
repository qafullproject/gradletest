package tests.junit5;

import Utilities.AppConfig;
import Utilities.JsonHelper;
import com.fasterxml.jackson.databind.annotation.JsonAppend;
import lombok.SneakyThrows;
import models.Settings;
import org.aeonbits.owner.ConfigFactory;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.Set;

@Tag("UNIT")
public class PropertiesReaderTests {
    private String fileName = "src/test/resources/project.properties/";

    @Test
    @SneakyThrows
    @Tag("SMOKE")
    public void simpleReaderTest(){
        Properties properties = new Properties();
        FileInputStream fs = new FileInputStream(fileName);
        properties.load(fs);

        String url = properties.getProperty("url");

        boolean isProduction = Boolean.parseBoolean(properties.getProperty("is_Production"));
        int threads = Integer.parseInt(properties.getProperty("threads"));

        System.out.println(url);
        System.out.println(isProduction);
        System.out.println(threads);
    }

    @Test
    @SneakyThrows
    public void jacksonTest(){
        Properties properties = new Properties();
        FileInputStream fs = new FileInputStream(fileName);
        properties.load(fs);

        String json = JsonHelper.toJson(properties);
        System.out.println(json);

        Settings settings = JsonHelper.fromJsonString(json, Settings.class);

        System.out.println(settings.getUrl());
        System.out.println(settings.getIsProduction());
        System.out.println(settings.getThreads());
    }

    @Test
    public void ownerReaderTest(){
        AppConfig appConfig = ConfigFactory.create(AppConfig.class);
        System.out.println(appConfig.url());
        System.out.println(appConfig.isProd());
        System.out.println(appConfig.threads());
    }
}
