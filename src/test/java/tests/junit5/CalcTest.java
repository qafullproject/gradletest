package tests.junit5;

import calc.CalcSteps;
import io.qameta.allure.Allure;
import io.qameta.allure.Issue;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicInteger;

public class CalcTest {

    @Test
    public void sumTest(){
        CalcSteps calcSteps = new CalcSteps();
        int result = calcSteps.sum(1,4);
        boolean isOk = calcSteps.isPositive(result);
        Assertions.assertTrue(isOk);
    }

    @Test
    @Issue("VIDEO - 32423523")
    public void sumStepsTest(){
        int a = 3;
        int b = 5;
        AtomicInteger result = new AtomicInteger();
        Allure.step("Прибавляем " + a + "к " + b, step -> {
            result.set(a + b);
        });

        Allure.step("Проверяем что результат " + result.get() + "больше нуля",  x->{
            Assertions.assertTrue(result.get() > 0);
        });
    }
}
