package tests.junit5.pageObjects.wildberies;

import com.codeborne.selenide.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import tests.junit5.pageObjects.wildberies.unitickets.UtMailSelenidePage;
import tests.junit5.pageObjects.wildberies.unitickets.UtMainPage;
import tests.junit5.selenoid.BrowserType;

import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.$x;

@Tag("UI")
@BrowserType(browser = BrowserType.Browser.CHROME, isRemote = true)
public class UtSelenideTests {


    @Test
    public void selenideTest(){
        int expectedDayForward = 27;
        int expectedDayBack = 30;

        Selenide.open("https://uniticket.ru/");

        SelenideElement header = $x("//h1");
        UtMailSelenidePage mainPage = new UtMailSelenidePage();
        mainPage.setCityFrom("Казань")
                .setCityTo("Дубай")
                .setDayForward(27)
                .setDayBack(30)
                .search()
                .waitForPage()
                .waitForTitleDisappear()
                .assertMainDayBack(expectedDayBack)
                .assertMainDayForward(expectedDayForward);
    }
}
