package tests.junit5.pageObjects.wildberies;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import tests.junit5.pageObjects.wildberies.unitickets.UtMainPage;
import tests.junit5.selenoid.BrowserType;

@Tag("UI")
@BrowserType(browser = BrowserType.Browser.CHROME, isRemote = true)
public class UrFilterTests extends BaseTest{

    @BeforeEach
    public void openSite(){
        driver.get("https://uniticket.ru/");
    }

    @Test
    public void filterTest(){
        int expectedDayForward = 27;
        int expectedBack = 30;
        UtMainPage mainPage = new UtMainPage(driver);
        mainPage.setCityFrom("Казань")
                .setCityTo("Дубай")
                .setDayForward(expectedDayForward)
                .setDayBack(expectedBack)
                .search();

    }

}
