package tests.junit5.pageObjects.wildberies;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import tests.junit5.pageObjects.wildberies.wbPages.ItemPage;
import tests.junit5.pageObjects.wildberies.wbPages.MainPage;
import tests.junit5.pageObjects.wildberies.wbPages.SearchResultPage;
import tests.junit5.selenoid.BrowserType;


@Tag("UI")
@BrowserType(browser = BrowserType.Browser.CHROME, isRemote = false)
public class WbFilterTests extends BaseTest {
    @BeforeEach
    public void openSite(){
        driver.get("https://www.wildberries.ru/");
    }

    @Test
    public void searchResultTest(){

        Integer minPrice = 80000;
        Integer maxPrice = 100000;

        String expectedItem = "iPhone 15";
        MainPage mainPage = new MainPage(driver);

        mainPage.searchItem(expectedItem);

        SearchResultPage resultPage = new SearchResultPage(driver);
        resultPage.openFilters();
        resultPage.setMinPrice(minPrice);
        resultPage.setMaxPrice(maxPrice);
        resultPage.applyFilters();
        resultPage.openItem();

        ItemPage itemPage = new ItemPage(driver);
        String actualmName = itemPage.getItemName();

        Integer actualPrice = itemPage.getItemPrice();

        Assertions.assertTrue(actualmName.toLowerCase().contains(expectedItem.toLowerCase()));
        Assertions.assertTrue(actualPrice >= minPrice && maxPrice <= actualPrice);
    }
}
