package tests.junit5.pageObjects.wildberies.unitickets;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import tests.junit5.pageObjects.wildberies.BasePage;
import tests.junit5.pageObjects.wildberies.BaseTest;

public class UtMainPage extends BasePage {
    private By cityFromField = By.xpath("//input[placeholder='Откуда']");
    private By listOfCityFrom = By.xpath("//div[@class ='origin field active']//div[@class = 'city']");
    private By cityToField = By.xpath("//input[@placeholder = 'Куда']");
    private final By listOfCityTo = By.xpath("//div[@class ='destination field active']//div[@class = 'city']");
    private By dateForward = By.xpath("//input[@placeholder = 'Туда']");
    private By dateBack = By.xpath("//input[@placeholder = 'Обратно']");

    private String dayInCalendar = "//span[text()='%d']";
    private By searchBtn = By.xpath("//div[class='search_btn']");
    public UtMainPage(WebDriver driver) {
        super(driver);
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(cityFromField));
        wait.until(ExpectedConditions.elementToBeClickable(searchBtn));
    }

    public UtMainPage setCityFrom(String city){
        driver.findElement(cityFromField).clear();
        driver.findElement(cityFromField).sendKeys(city);
        waitForTextPresentedInList(listOfCityFrom, city).click();

        return this;
    }

    public UtMainPage setCityTo(String city){
        driver.findElement(cityToField).clear();
        driver.findElement(cityToField).sendKeys(city);
        waitForTextPresentedInList(listOfCityTo, city).click();

        return this;
    }

    public UtMainPage setDayForward(int day){
            driver.findElement(dateForward);
            getDay(day).click();

            return this;
    }

    public UtMainPage setDayBack(int day){
        driver.findElement(dateBack);
        getDay(day).click();

        return this;
    }

    private WebElement getDay(int day){
        By dayLocator = By.xpath(String.format(dayInCalendar, day));

        return driver.findElement(dayLocator);
    }

    public void search(){
        driver.findElement(searchBtn).click();
    }

}
