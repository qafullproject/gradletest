package tests.junit5.pageObjects.wildberies.unitickets;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import tests.junit5.pageObjects.wildberies.BasePage;

public class UtSearchPage extends BasePage {

    private By titleLoader = By.xpath("//div[@class = 'countdown-title']");
    private By priceSelected = By.xpath("//li[@class = 'price-current']//span[@class = 'prices__price currency_font_currency_font--rub']");

    //private By
    public UtSearchPage(WebDriver driver) {
        super(driver);
    }
}
