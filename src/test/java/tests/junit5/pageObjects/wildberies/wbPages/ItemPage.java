package tests.junit5.pageObjects.wildberies.wbPages;

import org.openqa.selenium.*;
import tests.junit5.pageObjects.wildberies.BasePage;

public class ItemPage extends BasePage {
    private final By itemHeaderName = By.xpath("//h1[@data-jsv='#2494^/2494^']");
    private final By itemPrice = By.xpath("//span[@claas ='price-block__price']");

    public ItemPage(WebDriver driver) {
        super(driver);
    }

    public String getItemName(){
        return driver.findElement(itemHeaderName).getText();
    }

    public Integer getItemPrice(){
        WebElement itemPriceEl = driver.findElement(itemPrice);

        String priceText = getText(itemPrice);

        priceText = itemPriceEl.getText();
        priceText = priceText.replaceAll("[^0-9.]", "");

        return Integer.parseInt(priceText);
    }


}
