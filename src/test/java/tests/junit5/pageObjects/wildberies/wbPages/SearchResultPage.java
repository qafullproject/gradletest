package tests.junit5.pageObjects.wildberies.wbPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import tests.junit5.pageObjects.wildberies.BasePage;

public class SearchResultPage extends BasePage {
    private final By allFiltersBtn = By.xpath("//button[@class='dropdown-filter__btn dropdown-filter__btn--all']");
    private final By endPriceField = By.xpath("//input[@name='endN']");
    private final By startPriceField = By.xpath("//input[@name='startN']");
    private final By showBtn = By.xpath("//button[@data-jsv='#7930^/7930^']");

    private final By items = By.xpath("//div[@class = 'product-card-list']//article");

    public SearchResultPage(WebDriver driver) {
        super(driver);
    }

    public ItemPage openItem(){
        driver.findElements(items).get(0).click();
        waitUntilPageLoadsWb();

        return new ItemPage(driver);
    }
    public SearchResultPage openFilters(){
        driver.findElement(allFiltersBtn).click();
        return this;
    }

    public SearchResultPage setMinPrice(Integer minPrice){
        driver.findElement(startPriceField).clear();
        driver.findElement(startPriceField).sendKeys(String.valueOf(minPrice));
        return this;
    }

    public SearchResultPage setMaxPrice(Integer minPrice){
        driver.findElement(endPriceField).clear();
        driver.findElement(endPriceField).sendKeys(String.valueOf(minPrice));
        return this;
    }

    public SearchResultPage applyFilters(){
        driver.findElement(showBtn).click();
        return this;
    }


}
