package tests.junit5.api;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.RestAssured;
import io.restassured.common.mapper.TypeRef;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import listener.CustomTpl;
import models.fakeapiuser.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.testng.annotations.BeforeSuite;

import javax.jws.soap.SOAPBinding;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class SimpleApiRefactoredTests {
    @BeforeAll
    public static void setUp(){
        RestAssured.baseURI = "https://fakestoreapi.com";
        RestAssured.filters(new RequestLoggingFilter(), new ResponseLoggingFilter(),
                CustomTpl.customLogFilter().withCustomTemplates());
    }

    @Test
    public void getAllUser(){
        given().get("/users")
                .then().log().all()
                .statusCode(200);
    }

    @Test
    public void getSingleUser(){
        int userId  = 5;
        UserRoot response = given()
                .pathParam("userId",  userId)
                .get("/users/{userId}")
                .then()
                .statusCode(200)
                .extract().as(UserRoot.class);


        Assertions.assertEquals(userId, response.getId());
        Assertions.assertTrue(response.getAddress().getZipcode().matches("\\d{5}-\\d{4}"));
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 10, 20})
    public void getAllUsersWithLimitTest(int limitSize){
        List<UserRoot> users = given()
                .queryParam("limit", limitSize)
                .get("/users")
                .then()
                .statusCode(200)
                .extract().as(new TypeRef<List<UserRoot>>() {});

        Assertions.assertEquals(limitSize, users.size());
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 30})
    public void getAllUsersWithLimitErrorParams(int limitSize){
        List<UserRoot> users = given()
                .queryParam("limit", limitSize)
                .get("/users")
                .then()
                .statusCode(200)
                .extract().as(new TypeRef<List<UserRoot>>() {});

        Assertions.assertNotEquals(limitSize, users.size());
    }

    @Test
    public void getAllUsersSortByDesc(){
        String sortType = "desc";
        List<UserRoot> usersSorted = given()
                .queryParam("sort", sortType)
                .get("/users")
                .then()
                .extract().as(new TypeRef<List<UserRoot>>() {});

        List<UserRoot> notSortedUsers = given()
                .get("/users")
                .then()
                .extract().as(new TypeRef<List<UserRoot>>() {});

        List<Integer> sortedResponseIds = usersSorted
                .stream()
                .map(UserRoot :: getId).collect(Collectors.toList());

        List<Integer> sortedByCode = notSortedUsers.stream()
                .map(UserRoot::getId)
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.toList());


        Assertions.assertNotEquals(usersSorted, notSortedUsers);

        Assertions.assertEquals(sortedByCode, sortedResponseIds);
    }


    private UserRoot getTestUser(){
        Name name = new Name("Thomas", "Shelby");
        Geolocation geolocation = new Geolocation("-31.232.2", "-33.223.22");
        Address address = Address.builder()
                .city("Paris")
                .number(100)
                .zipcode("12333-1233")
                .street("Novyi Arbat 12")
                .geolocation(geolocation)
                .build();

        return  UserRoot.builder()
                .name(name)
                .address(address)
                .phone("343434343")
                .email("sdsd@gmail.ee")
                .password("password")
                .username("sdsdsdsdsdsdsdsd")
                .build();
    };

    @Test
    public void addNewUserTest(){
        UserRoot user = getTestUser();


        Integer userId = given().body(user)
                .post("/users")
                .then()
                .statusCode(200)
                .extract().jsonPath().getInt("id");

        Assertions.assertNotNull(userId);

    }

    @Test
    public void updateUserTest(){
        UserRoot user = getTestUser();
        String oldPassword = user.getPassword();
        user.setPassword("newpass111");

        UserRoot userUpdate =
                given()
                        .body(user)
                        .pathParams("userId", user.getId())
                        .put("/users/{userId}")
                        .then()
                        .extract().as(UserRoot.class);

        Assertions.assertNotEquals(userUpdate.getPassword(), oldPassword);
    }

    @Test
    public void authUserTest(){
        AuthData authData = new AuthData("jimie_k", "klein*#%*");

        String token = given().contentType(ContentType.JSON).body(authData).post("/auth/login")
                .then()
                .extract().jsonPath().getString("token");

        Assertions.assertNotNull(token);
    }



}
