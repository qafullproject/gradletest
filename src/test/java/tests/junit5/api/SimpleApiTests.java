package tests.junit5.api;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import models.fakeapiuser.Address;
import models.fakeapiuser.Geolocation;
import models.fakeapiuser.Name;
import models.fakeapiuser.UserRoot;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class SimpleApiTests {
    @Test
    public void getAllUsersTest(){
        given().get("https://fakestoreapi.com/users")
                .then()
                .log().all()
                .statusCode(200);
    }

    @Test
    public void getSingleUser(){
        int userId  = 2;
        given().pathParams("userId",  userId)
                .get("https://fakestoreapi.com/users/{userId}")
                .then().log().all()
                .statusCode(200)
                .body("id", equalTo(userId))
                .body("address.zipcode", matchesPattern("\\d{5}-\\d{4}"));
    }

    @Test
    public void getAllUsersWithLimitTest(){
        int limitSize = 3;
        given().queryParam("limit", limitSize)
                .get("https://fakestoreapi.com/users")
                .then().log().all()
                .statusCode(200)
                .body("", hasSize(limitSize));
    }

    @Test
    public void getAllUsersSortByDesc(){
        String sortType = "desc";
        Response sortedResponse = given().queryParam("sort", sortType)
                .get("https://fakestoreapi.com/users")
                .then().log().all()
                .extract().response();

        Response notSortedResponse = given()
                .get("https://fakestoreapi.com/users")
                .then().log().all().extract().response();

        List<Integer> sortedResponseIds = sortedResponse.jsonPath().getList("id");
        List<Integer> notSortedResponseIds = notSortedResponse.jsonPath().getList("id");

        List<Integer> sortedByCode = notSortedResponseIds.stream()
                        .sorted(Comparator.reverseOrder()).collect(Collectors.toList());


        Assertions.assertNotEquals(sortedResponseIds, notSortedResponseIds);

        Assertions.assertEquals(sortedByCode, sortedResponseIds);
    }

    @Test
    public void addNewUserTest(){
        Name name = new Name("Thomas", "Shelby");
        Geolocation geolocation = new Geolocation("-31.232.2", "-33.223.22");
        Address address = Address.builder()
                .city("Paris")
                .number(100)
                .zipcode("12333-1233")
                .street("Novyi Arbat 12")
                .geolocation(geolocation)
                .build();

        UserRoot requestBody = UserRoot.builder()
                .name(name)
                .address(address)
                .phone("343434343")
                .email("sdsd@gmail.ee")
                .password("password")
                .username("sdsdsdsdsdsdsdsd")
                .build();

        given().body(requestBody)
                .post("https://fakestoreapi.com/users").then().log().all();
    }

    private UserRoot getTestUser(){
        Name name = new Name("Thomas", "Shelby");
        Geolocation geolocation = new Geolocation("-31.232.2", "-33.223.22");
        Address address = Address.builder()
                .city("Paris")
                .number(100)
                .zipcode("12333-1233")
                .street("Novyi Arbat 12")
                .geolocation(geolocation)
                .build();

        return  UserRoot.builder()
                .name(name)
                .address(address)
                .phone("343434343")
                .email("sdsd@gmail.ee")
                .password("password")
                .username("sdsdsdsdsdsdsdsd")
                .build();
    };

    @Test
    public void updateUserTest(){
        UserRoot user = getTestUser();
        String oldPassword = user.getPassword();

        user.setPassword("newpass111");

        given().body(user).put("https://fakestoreapi.com/users/" + user.getId())
                .then().log().all()
                .body("password", not(equalTo(oldPassword)));
    }

    @Test
    public void deleteTestUser(){
        given().delete("https://fakestoreapi.com/users/7")
                .then().log().all()
                .statusCode(200);
    }

    @Test
    public void authUserTest(){
        Map<String, String> userAuth = new HashMap<>();
        userAuth.put("username", "jimie_k");
        userAuth.put("password", "klein*#%*");

        given().contentType(ContentType.JSON).body(userAuth).post("https://fakestoreapi.com/auth/login")
                .then().log().all();
    }

}
