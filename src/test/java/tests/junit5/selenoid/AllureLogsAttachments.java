package tests.junit5.selenoid;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import io.qameta.allure.Attachment;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.nio.charset.StandardCharsets;

public class AllureLogsAttachments {

    private static final String SELENOID_HOST = System.getenv("host").replaceAll("/wd/hub", "");

    @Attachment(value = "Page source", type = "text/plain")
    public static byte[] pageSource(){
        return WebDriverRunner.getWebDriver().getPageSource().getBytes(StandardCharsets.UTF_8);
    }

    @Attachment(value = "Page screen", type = "image/png")
    public static byte[] pageScreen(){
        return  ((TakesScreenshot) WebDriverRunner.getWebDriver()).getScreenshotAs(OutputType.BYTES);
    }

    public static String getLogs(){
        String browser = ((RemoteWebDriver)WebDriverRunner.getWebDriver()).getCapabilities().getBrowserName();

        if(browser.equals("chrome"))
            return String.join("\n", Selenide.getWebDriverLogs(LogType.BROWSER));

        return null;
    }

}
