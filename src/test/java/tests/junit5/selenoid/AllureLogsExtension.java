package tests.junit5.selenoid;

import org.junit.jupiter.api.extension.AfterTestExecutionCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

public class AllureLogsExtension implements AfterTestExecutionCallback {
    @Override
    public void afterTestExecution(ExtensionContext context) throws Exception {
        context.getExecutionException().ifPresent(x->{
            AllureLogsAttachments.pageSource();
            AllureLogsAttachments.pageScreen();
            AllureLogsAttachments.getLogs();
        });
    }
}
