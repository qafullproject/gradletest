package tests.junit5.selenoid;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

import static com.codeborne.selenide.Selenide.$x;


public class selenoidChromeTests {


    @Test
    @BrowserType(browser = BrowserType.Browser.FIREFOX, isRemote = true)
    public void selenoidTest(){
        Selenide.open("https://vk.com");
        $x("//div[@class = 'login_mobile_header']").should(Condition.text("wrong value"));
    }

    @Test
    @BrowserType(browser = BrowserType.Browser.CHROME, isRemote = true)
    public void selenoidTest2(){
        Selenide.open("https://vk.com");
    }
}
