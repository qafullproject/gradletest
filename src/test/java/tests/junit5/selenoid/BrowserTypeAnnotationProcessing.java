package tests.junit5.selenoid;

import com.codeborne.selenide.*;
import org.bouncycastle.oer.Switch;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class BrowserTypeAnnotationProcessing implements BeforeAllCallback, BeforeEachCallback {

    private static ExtensionContext.Namespace space = ExtensionContext.Namespace
            .create(BrowserTypeAnnotationProcessing.class);

    @Override
    public void beforeAll(ExtensionContext context) throws Exception {
        BrowserType annotation = context.getRequiredTestClass().getAnnotation(BrowserType.class);
        context.getStore(space).put("annotation", annotation);
    }

    private Capabilities getCaps(BrowserType type){
        switch (type.browser()){
            case FIREFOX:
                FirefoxOptions firefoxOptions = new FirefoxOptions();
                firefoxOptions.setCapability("browserVersion", "119.0");
                return firefoxOptions;
            case CHROME:
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.setCapability("browserVersion", "120.0");
                return chromeOptions;
            default: throw new IllegalArgumentException("Bad browser type");
        }
    }

    @Override
    public void beforeEach(ExtensionContext context) throws Exception {
        BrowserType methodAnnotation = context.getRequiredTestMethod().getAnnotation(BrowserType.class);
        BrowserType classAnnotation = context.getStore(space).get("annotation", BrowserType.class);

        if(methodAnnotation == null)
            methodAnnotation = classAnnotation;

        if(methodAnnotation != null)
            if(methodAnnotation.isRemote()){

                RemoteWebDriver remoteWebDriver = createSelenoidDriver(methodAnnotation);
                WebDriverRunner.setWebDriver(remoteWebDriver);
            } else{
               WebDriver localWebDriver = createLocalDriver(methodAnnotation);
               WebDriverRunner.setWebDriver(localWebDriver);
            }
    }


    private RemoteWebDriver createSelenoidDriver(BrowserType browserType) throws MalformedURLException{
        DesiredCapabilities capabilities = new DesiredCapabilities();
        Map<String, Object> selenoidOptions = new HashMap<>();
        selenoidOptions.put("enableVNC", true);
        selenoidOptions.put("enableVideo", false);
        capabilities.setCapability("selenoid:options", selenoidOptions);
        capabilities.merge(getCaps(browserType));

        RemoteWebDriver remoteWebDriver = new RemoteWebDriver(new URL(System.getenv("host")), capabilities);
        remoteWebDriver.setFileDetector(new LocalFileDetector());
        return remoteWebDriver;
    }

    private WebDriver createLocalDriver(BrowserType browserTpe){
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--no-sandbox");
        options.addArguments("--headless");
        SelenideConfig config = new SelenideConfig()
                .browser(browserTpe.browser().name().toLowerCase())
                .browserCapabilities(options);

        SelenideDriver driver = new SelenideDriver(config);
        return driver.getAndCheckWebDriver();
    }
}
