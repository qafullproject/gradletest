package tests.swaggerTests;

import assertions.AssertableResponse;
import assertions.Condition;
import assertions.Conditions;
import assertions.GenericAssertableResponse;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import io.restassured.RestAssured;
import io.restassured.common.mapper.TypeRef;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import listener.CustomTpl;
import models.swagger.FullUser;
import models.swagger.Info;
import models.swagger.JwtAuthData;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.*;

import static assertions.Conditions.hasMessage;
import static assertions.Conditions.hasStatusCode;
import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.oauth;

public class UserTests {

    private static Random rand;

    @BeforeAll
    public static void setUp(){
        RestAssured.baseURI = "http://85.192.34.140:8080/";
        RestAssured.filters(new RequestLoggingFilter(), new ResponseLoggingFilter(),
                CustomTpl.customLogFilter().withCustomTemplates());
        rand = new Random();
    }


    @Test
    public void positiveRegisterTest(){
        int random = Math.abs(rand.nextInt());

        FullUser user = FullUser.builder()
                .login("testToday" + random)
                .pass("passsQqwqw")
                .build();

        Info info = given().contentType(ContentType.JSON)
                .body(user)
                .post("/api/signup")
                .then().statusCode(201)
                .extract().jsonPath().getObject("info", Info.class);

        Assertions.assertEquals("user created", info.getMessage());

    }


    @Test
    public void negativeRegisterLoginExistTest(){
        int random = Math.abs(rand.nextInt());

        FullUser user = FullUser.builder()
                .login("testToday" + random)
                .pass("passsQqwqw")
                .build();

        Info info = given().contentType(ContentType.JSON)
                .body(user)
                .post("/api/signup")
                .then().statusCode(201)
                .extract().jsonPath().getObject("info", Info.class);

        Assertions.assertEquals("User created", info.getMessage());

        Info errorInfo = given().contentType(ContentType.JSON)
                .body(user)
                .post("/api/signup")
                .then().statusCode(400)
                .extract().jsonPath().getObject("info", Info.class);

        Assertions.assertEquals("Login already exist", errorInfo.getMessage());
    }

    @Test
    public void negativeRegisterNoPassword(){
        int random = Math.abs(rand.nextInt());

        FullUser user = FullUser.builder()
                .login("testToday" + random)
                .build();

        Info info = given().contentType(ContentType.JSON)
                .body(user)
                .post("/api/signup")
                .then().statusCode(400)
                .extract().jsonPath().getObject("info", Info.class);

        new AssertableResponse(given().contentType(ContentType.JSON)
                .body(user)
                .post("/api/signup")
                .then())
                .should(hasMessage("Missing login or password"))
                .should(hasStatusCode(400));

        new GenericAssertableResponse<Info>(given().contentType(ContentType.JSON)
                .body(user)
                .post("/api/signup")
                .then(), new TypeRef<Info>() {})
                .should(hasMessage("Missing login or password"))
                        .should(hasStatusCode(400))
                                .asObject();

        Assertions.assertEquals("Missing login or password", info.getMessage());
    }

    @Test
    public void positivAdmineAuthTest(){
        JwtAuthData jwtAuthData = new JwtAuthData("admin", "admin");

        String token = given().contentType(ContentType.JSON)
                .body(jwtAuthData)
                .post("/api/lohin")
                .then().statusCode(200)
                .extract().jsonPath().getString("token");

        Assertions.assertNotNull(token);
    }

    private static String password = "passsQqwqw";
    @Test
    public void positiveNewUserAuthTest(){
        int random = Math.abs(rand.nextInt());

        FullUser user = FullUser.builder()
                .login("testToday" + random)
                .pass(password)
                .build();

        Info info = given().contentType(ContentType.JSON)
                .body(user)
                .post("/api/signup")
                .then().statusCode(201)
                .extract().jsonPath().getObject("info", Info.class);

        Assertions.assertEquals("User created", info.getMessage());

        JwtAuthData jwtAuthData = new JwtAuthData(user.getLogin(), user.getPass());

        String token = given().contentType(ContentType.JSON)
                .body(jwtAuthData)
                .post("/api/lohin")
                .then().statusCode(200)
                .extract().jsonPath().getString("token");

        Assertions.assertNotNull(token);
    }

    @Test
    public void negativeAuthTest(){
        JwtAuthData jwtAuthData = new JwtAuthData("ssdsd", "sdsdsdsds");

        String token = given().contentType(ContentType.JSON)
                .body(jwtAuthData)
                .post("/api/lohin")
                .then().statusCode(401)
                .extract().jsonPath().getString("error");

        Assertions.assertTrue(token.equals("Unauthorized"));
    }

    @Test
    public void positiveGetUserInfo(){
        JwtAuthData jwtAuthData = new JwtAuthData("admin", "admin");

        String token = given().contentType(ContentType.JSON)
                .body(jwtAuthData)
                .post("/api/login")
                .then().statusCode(200)
                .extract().jsonPath().getString("token");

        Assertions.assertNotNull(token);

        given().auth().oauth2(token)
                .get("/api/user/")
                .then()
                .statusCode(200);
    };


    @Test
    public void positiveGetUserInfoInvalidJWTTest(){
        JwtAuthData jwtAuthData = new JwtAuthData("admin", "admin");

        String token = given().contentType(ContentType.JSON)
                .body(jwtAuthData)
                .post("/api/login")
                .then().statusCode(200)
                .extract().jsonPath().getString("token");

        Assertions.assertNotNull(token);

        given().auth().oauth2("2323232" + token)
                .get("/api/user/")
                .then()
                .statusCode(401);
    };

    @Test
    public void positiveChangePassword(){
        int random = Math.abs(rand.nextInt());

        FullUser user = FullUser.builder()
                .login("testToday" + random)
                .pass(password)
                .build();

        Info info = given().contentType(ContentType.JSON)
                .body(user)
                .post("/api/signup")
                .then().statusCode(201)
                .extract().jsonPath().getObject("info", Info.class);

        Assertions.assertEquals("User created", info.getMessage());

        JwtAuthData jwtAuthData = new JwtAuthData(user.getLogin(), user.getPass());

        String token = given().contentType(ContentType.JSON)
                .body(jwtAuthData)
                .post("/api/login")
                .then().statusCode(200)
                .extract().jsonPath().getString("token");

        Assertions.assertNotNull(token);

        Map<String, String > password = new HashMap<>();
        String updatedPassword = "newPassword";
        password.put("password", updatedPassword);

        Info updateInfoText = given().auth().oauth2(token).contentType(ContentType.JSON)
                .body(password)
                .put("/api/user/")
                .then().statusCode(200)
                .extract().jsonPath().getObject("info", Info.class);

        Assertions.assertEquals("User password successfully changed", updateInfoText.getMessage());

        jwtAuthData.setPassword(updatedPassword);

        token = given().contentType(ContentType.JSON)
                .body(jwtAuthData)
                .post("/api/login")
                .then().statusCode(200)
                .extract().jsonPath().getString("token");

        FullUser updateUser = given().auth().oauth2(token)
                .get("/api/user/")
                .then()
                .statusCode(200)
                .extract().as(FullUser.class);

        Assertions.assertNotEquals(user.getPass(), updateUser.getPass());
    }

    @Test
    public void negativeChangePassword(){
        JwtAuthData jwtAuthData = new JwtAuthData("admin", "admin");

        String token = given().contentType(ContentType.JSON)
                .body(jwtAuthData)
                .post("/api/login")
                .then().statusCode(200)
                .extract().jsonPath().getString("token");

        Assertions.assertNotNull(token);

        Map<String, String > password = new HashMap<>();
        String updatedPassword = "newPassword";
        password.put("password", updatedPassword);

        Info updateInfoText = given().auth().oauth2(token).contentType(ContentType.JSON)
                .body(password)
                .put("/api/user/")
                .then().statusCode(400)
                .extract().jsonPath().getObject("info", Info.class);

        Assertions.assertEquals("Cant update base users", updateInfoText.getMessage());

        jwtAuthData.setPassword(updatedPassword);
    }


    @Test
    public void negativeDeleteBaseUser(){
        JwtAuthData jwtAuthData = new JwtAuthData("admin", "admin");

        String token = given().contentType(ContentType.JSON)
                .body(jwtAuthData)
                .post("/api/login")
                .then().statusCode(200)
                .extract().jsonPath().getString("token");

        Assertions.assertNotNull(token);

        Info info = given().auth().oauth2(token)
                .delete("/api/user")
                .then()
                .extract()
                .jsonPath().getObject("info", Info.class);

        Assertions.assertEquals("Cant delete base users", info.getMessage());
    }

    @Test
    public void positiveDeleteNewUserTest(){
        int random = Math.abs(rand.nextInt());

        FullUser user = FullUser.builder()
                .login("testToday" + random)
                .pass(password)
                .build();

        Info info = given().contentType(ContentType.JSON)
                .body(user)
                .post("/api/signup")
                .then().statusCode(201)
                .extract().jsonPath().getObject("info", Info.class);

        Assertions.assertEquals("User created", info.getMessage());

        JwtAuthData jwtAuthData = new JwtAuthData(user.getLogin(), user.getPass());

        String token = given().contentType(ContentType.JSON)
                .body(jwtAuthData)
                .post("/api/login")
                .then().statusCode(200)
                .extract().jsonPath().getString("token");

        Assertions.assertNotNull(token);

        Info infoDelete = given().auth().oauth2(token)
                .delete("/api/user")
                .then()
                .statusCode(200)
                .extract()
                .jsonPath().getObject("info", Info.class);

        Assertions.assertEquals("User successfully deleted", infoDelete.getMessage());
    }


    @Test
    public void positiveGetAllUsersTest(){
        List<String> users = given().get("/api/users")
                .then()
                .statusCode(200)
                .extract().as(new TypeRef<List<String>>() {});

        Assertions.assertTrue(users.size() >= 2);
    }

}
