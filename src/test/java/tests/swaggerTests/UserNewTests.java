package tests.swaggerTests;

import io.restassured.RestAssured;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.response.Response;
import listener.AdminUser;
import listener.AdminUserResolver;
import listener.CustomTpl;
import models.swagger.FullUser;
import models.swagger.Info;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import services.UserService;
import java.util.List;
import java.util.Random;

import static Utilities.RandomTestData.*;
import static assertions.Conditions.hasMessage;
import static assertions.Conditions.hasStatusCode;

@ExtendWith(AdminUserResolver.class)
public class UserNewTests {
    private static Random rand;
    private static UserService userService;
    private FullUser user;

    @BeforeEach
    public void initTestUser(){
        user = getRandomUser();
    }

    @BeforeAll
    public static void setUp(){
        RestAssured.baseURI = "http://85.192.34.140:8080/";
        RestAssured.filters(new RequestLoggingFilter(), new ResponseLoggingFilter(),
                CustomTpl.customLogFilter().withCustomTemplates());
        rand = new Random();
        userService = new UserService();
    }


    @Test
    public void positiveRegisterTest(){
        userService.register(user)
                .should(hasStatusCode(201))
                .should(hasMessage("User created"));

    }

    @Test
    public void positiveRegisterTestWithGames(){
        FullUser user = getRandomUserWithGames();

        Response response = userService.register(user)
                //.should(hasStatusCode(201))
                //.should(hasMessage("User created"))
                .asResponse();

        Info info = response.jsonPath().getObject("info", Info.class);

        SoftAssertions softAssertions = new SoftAssertions();
        softAssertions.assertThat(response.statusCode()).as("Статус код не был 200")
                .isEqualTo(200);

        softAssertions.assertThat(info.getMessage()).as("Сообщение об ошибке было не верное")
                .isEqualTo("фейк месседж");

        softAssertions.assertAll();
    }

    @Test
    public void negativeRegisterLoginExistTest(){
        userService.register(user);

        userService.register(user)
                .should(hasStatusCode(400))
                .should(hasMessage("Login already exist"));
    }

    @Test
    public void negativeRegisterNoPassword(){
        user.setPass(null);

        userService.register(user)
                .should(hasStatusCode(400))
                .should(hasMessage("Missing login or password"));
    }


    @Test
    public void positivAdmineAuthTest(){
        FullUser user = getAdminUser();

        String token = userService.auth(user)
                .should(hasStatusCode(200))
                .asJwt();

        Assertions.assertNotNull(token);
    }

    private static String password = "passsQqwqw";
    @Test
    public void positiveNewUserAuthTest(){
       userService.register(user);
       String token = userService.auth(user)
               .should(hasStatusCode(200))
               .asJwt();

        Assertions.assertNotNull(token);
    }

    @Test
    public void negativeAuthTest(){
        userService.auth(user)
                .should(hasStatusCode(401));
    }

    @Test
    public void positiveGetUserInfo(@AdminUser FullUser admin){
        String token = userService.auth(admin).asJwt();

        userService.getUserInfo(token)
                        .should(hasStatusCode(200));
    };


    @Test
    public void positiveGetUserInfoInvalidJWTTest(){
        userService.getUserInfo("asds ").should(hasStatusCode(401));
    };

    @Test
    public void positiveChangePassword(){
        String oldPassword = user.getPass();
        userService.register(user)
                .should(hasStatusCode(201))
                .should(hasMessage("User created"));

        String token = userService.auth(user).asJwt();

        String updatedPassword = "newPassword";

        userService.updatePass(updatedPassword, token)
                .should(hasStatusCode(200))
                .should(hasMessage("User password successfully changed"));

        user.setPass(updatedPassword);

        token = userService.auth(user).should(hasStatusCode(200)).asJwt();

        FullUser updateUser = userService.getUserInfo(token)
                        .as(FullUser.class);

        Assertions.assertNotEquals(oldPassword, updateUser.getPass());
    }

    @Test
    public void negativeChangePassword(){
        FullUser admin = getAdminUser();

        String token = userService.auth(admin).asJwt();

        String updatedPassword = "newPassword";

        userService.updatePass(updatedPassword, token)
                .should(hasStatusCode(400))
                .should(hasMessage("Cant update base users"));
    }


    @Test
    public void negativeDeleteBaseUser(){
        FullUser admin = getAdminUser();

        String token = userService.auth(admin).asJwt();

        Assertions.assertNotNull(token);

        userService.deleteUser(token)
                .should(hasMessage("Cant delete base users"))
                .should(hasStatusCode(400));
    }

    @Test
    public void positiveDeleteNewUserTest(){
        userService.register(user);

        String token = userService.auth(user).asJwt();
        Assertions.assertNotNull(token);

        userService.deleteUser(token)
                .should(hasStatusCode(200))
                .should(hasMessage("User successfully deleted"));
    }


    @Test
    public void positiveGetAllUsersTest(){
        List<String> users = userService.getAllUsers().asList(String.class);

        Assertions.assertTrue(users.size() >= 2);
    }
}
