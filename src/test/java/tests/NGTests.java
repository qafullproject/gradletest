package tests;

import calc.CalcSteps;
import listener.RetryListenerTestNG;
import models.People;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestNGMethod;
import org.testng.annotations.*;
import tests.testNG.DataTestArguments;

import java.util.*;

@Listeners(RetryListenerTestNG.class)
public class NGTests {

    @BeforeSuite
    public void setAnalyzer(ITestContext context){
        for (ITestNGMethod testMethod : context.getAllTestMethods()) {
            testMethod.setRetryAnalyzer(new RetryListenerTestNG());
        }
    }

    @AfterSuite
    private void saveFailed(){
        RetryListenerTestNG.saveFailedTests();
    }

    @Test(groups = {"sum1"})
    public void sumTestNGTest(){
        CalcSteps calcSteps = new CalcSteps();
        Assert.assertTrue(calcSteps.isPositive(-1));
    }


    @Test()
    public void sumTestNGTes2t(){
        CalcSteps calcSteps = new CalcSteps();
        Assert.assertTrue(calcSteps.isPositive(-1));
    }


    @DataProvider(name = "testUsers")
    public Object[] dataWithUsers(){
        People stas = new People("Stas", 24, "male");
        People katya = new People("Katya", 20, "female");
        People pleg = new People("Kaya", 20, "male");
        return new Object[]{stas, katya, pleg};
    }
    
    @Test(dataProvider = "testUsers")
    public void testUsersWithRole(People people){
        System.out.println(people.getName());
        Assert.assertTrue(people.getAge() > 18);

        Assert.assertTrue(people.getName().contains("aa"));
    }

    @DataProvider(name = "ips")
    public Object[][] testAddresses(){
        List<String> ips = new ArrayList<>();
        ips.add("123.3.2.1");
        ips.add("loalhost");
        ips.add("68.34.232.1");

        return ips.stream().map(x -> new Object[]{x})
                .toArray(Object[][]::new);
    }

    @Test(dataProvider = "ips")
    public void ipsTest(String ip){
        System.out.println(ip);
        Assert.assertTrue(ip.matches("^([0-9]+(\\.|$)){4}]"));
    }

    @Test(dataProviderClass = DataTestArguments.class, dataProvider = "argsForCalc")
    public void calcTest(int a, int b, int c){
        Assert.assertEquals(a + b, c);

    }

    @Test(dataProviderClass = DataTestArguments.class, dataProvider = "diffArgs")
    public void someMagicTransform(int a, String b){
        Assert.assertEquals(convert(a), b);
    }

    private String convert(int a){
        switch (a){
            case 1:
                return "one";
            case 4:
                return "four";
            default:
                return null;
        }
    }
}
