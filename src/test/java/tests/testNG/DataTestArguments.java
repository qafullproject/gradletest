package tests.testNG;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class DataTestArguments {

    @DataProvider(name = "argsForCalc")
    public Object[][] calcData() {
        return new Object[][]{{1, 4, 7}, {4, 3, 7}, {4, 7, 11}, {1, 3, 3}};
    }

    @DataProvider(name = "diffArgs")
    public Object[][] diffArgsObjects() {
        return new Object[][]{{1, "one"}, {4, "four"}
        };
    }



}
